## 概述

此仓库托管[织信低代码平台](https://informat.cn/)功能示例和典型使用场景，您可以下载工程中(Release)应用包，然后在织信平台安装。

安装文档：https://next.informat.cn/doc/guide/install/

另外织信也提供了在线体验环境，[体验环境](https://demo.informat.cn/web0/website/t1zubsnrtp8zu/yid101v7lewcc/ifc-cxuegdwwwk3qp.ifc.html)

## demo应用的功能清单

本应用为功能演示应用，旨在通过具体的功能向使用者直观的呈现织信低代码平台的核心功能模块，帮助使用者快速了解平台功能和典型的使用场景，快速入门。本应用中的一级目录是平台中大的功能板块，大家可以按照自己感兴趣的内容进入目录中进行深入学习。对于初次接触的同学，我们建议按如下的顺序来学习了解织信：

| **序号** | **板块** | **学习目标**                                  |
|--------|--------|-------------------------------------------|
| 1      | 数据和表单  | 了解模块的概念和学习如何使用织信进行模型搭建                    |
| 2      | 数据视图   | 学习视图的概念和各种视图的典型使用场景                       |
| 3      | 仪表盘    | 学习通过织信平台将数据进行可视化呈现                        |
| 4      | 表达式    | 理解表达式的概念，学会使用表达式衔接各功能间的数据流转，这是学习后续其他功能的基础 |
| 5      | 自动化    | 掌握常用自动化步骤的用法，学习如何使用自动化搭建具体功能              |
| 6      | 脚本     | 了解脚本的工作原理，进行学习如何使用脚本进行深度功能开发和平台功能扩展       |
| 7      | 工作流    | 了解常用流程图形的含义和配置内容，具备搭建简单流程的能力              |
| 8      | 角色权限   | 了解织信平台的成员管理功能和权限控制方式                      |

在按上述流程学习后，就已经具备了使用织信平台搭建应用的能力，在实操过程中如果对于如API、定时任务等其他的功能板块感兴趣可以进入具体模块中进行学习。接下来就让我们进入应用，开始我们的学习之旅吧。

## 应用截图

*** 应用概览 ***

![应用概览](./ressources/images/demo-app/app-summary.png "应用概览")

*** 基础数据表模型 ***

![基础数据表模型](./ressources/images/demo-app/app-table-basic-mode.png "基础数据表模型")

![子对象](./ressources/images/demo-app/app-table-basic-tree.png "子对象")

![函数字段](./ressources/images/demo-app/app-table-basic-formual.png "函数字段")

*** 多样性数据视图 ***

![个性化表格视图](./ressources/images/demo-app/app-table-view-styled.png "个性化表格视图")

![树形视图](./ressources/images/demo-app/app-table-view-tree.png "树形视图")

![甘特图视图](./ressources/images/demo-app/app-table-view-gantt.png "甘特图视图")

![看板视图](./ressources/images/demo-app/app-table-view-kanban.png "看板视图")

*** 丰富仪表盘图表 ***

![数据大屏](./ressources/images/demo-app/app-dashboard-chart-screen.png "数据大屏")

*** 自动化 ***

![自动化](./ressources/images/demo-app/app-automatic-graph.png "自动化")

![自动化步骤](./ressources/images/demo-app/app-automatic-func.png "自动化步骤")

*** 脚本编程支持 ***

![脚本支持](./ressources/images/demo-app/app-script.png "脚本支持")

*** 流程编辑器 ***

![流程编辑器](./ressources/images/demo-app/app-bpmn-graph.png "流程编辑器")

*** 自定义组件编辑 ***

![自定义组件](./ressources/images/demo-app/app-component-designer.png "自定义组件")

*** 主题样式 ***

![主题样式](./ressources/images/demo-app/app-theme-style.png "主题样式支持")

*** 打印模板 ***

![打印模板](./ressources/images/demo-app/app-print-template.png "打印模板")

## 应用安装

*** 安装包文件安装 ***

![安装包文件安装](./ressources/images/demo-app/app-install-imr.png "应用安装")

*** 安装包URL地址安装 ***

![安装包URL地址安装](./ressources/images/demo-app/app-install-url.png "应用安装")
